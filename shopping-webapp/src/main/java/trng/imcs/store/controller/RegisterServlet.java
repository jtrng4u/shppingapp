package trng.imcs.store.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import trng.imcs.store.beans.User;
import trng.imcs.store.mails.Mailer;
import trng.imcs.store.service.AuthUserService;
import trng.imcs.store.service.AuthUserServiceImpl;
import trng.imcs.store.service.ServiceException;
import trng.imcs.store.util.GenerateUUID;

/**
 * Servlet implementation class RegisterServlet
 */
@SuppressWarnings("serial")
public class RegisterServlet extends HttpServlet {

	final static Logger logger = Logger.getLogger(RegisterServlet.class);

	AuthUserService authUserService = null;
	String forwardUrl;
	User user = null;
	String emailText = null;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	public RegisterServlet() {
		authUserService = new AuthUserServiceImpl();
	}
	
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init();
		emailText = new StringBuilder("Please click the below URL to confirm your registration in IMCS Store.")
				.append(config.getServletContext().getContextPath()).append("/activate?key=").toString();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		User user = registerUser(request);

		if (user != null) {
			int userID = user.getUserId(); //getUserIDActivation(request);
			String emailID = user.getEmail(); //getUserEmail(userID)
			long uid = GenerateUUID.generateUUID();
			insertActivationDetails(userID, String.valueOf(uid));
//			sendMessage(uid, emailID);
			forwardUrl = "/userRegister.jsp";

		} else {
			forwardUrl = "/userLogin.jsp";
		}

		RequestDispatcher rd = request.getRequestDispatcher(forwardUrl);
		rd.forward(request, response);
	}

	private String getUserEmail(int userID) {
//		User user = authUserService.getUserById(userID);
		return user.getEmail();
	}

	private void sendMessage(long uid, String emailID) {
		StringBuilder emailTextBuilder = new StringBuilder(emailText);
		String str_emailMessageLink = emailTextBuilder.append(uid).toString();
		Mailer m = new Mailer();
		m.setPropsAndSendEmail(emailID, str_emailMessageLink);
	}

	private boolean insertActivationDetails(int userID, String emailUID) {
		try {
			return authUserService.insertActivationDetails(userID, emailUID);
		} catch (ServiceException e) {
			logger.error("Failed to insertActivationDetails", e);
			return false;
		}
	}

	private int getUserIDActivation(HttpServletRequest request) {
		String userName = request.getParameter("username");
		String password = request.getParameter("password");
		User user = null;
		try {
			user = authUserService.getByUsername(userName, password);
		} catch (ServiceException e) {
			logger.error("Failed to getUserIDActivation", e);
		}
		return user.getUserId();
	}

	private User registerUser(HttpServletRequest request) {
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String email = request.getParameter("email");
		String phone = request.getParameter("phone");
		
		User user = new User(null, username, password, email, phone, 1);
		try {
			return authUserService.saveUser(user);
		} catch (ServiceException e) {
			logger.error("Failed to registerUser", e);
			return null;
		}
	}
}

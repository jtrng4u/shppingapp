package trng.imcs.store.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.Statement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import trng.imcs.store.service.AuthUserService;
import trng.imcs.store.service.AuthUserServiceImpl;
import trng.imcs.store.service.ServiceException;

/**
 * Servlet implementation class MailActivationServlet
 */
public class MailActivationServlet extends HttpServlet {
	private static final long	serialVersionUID	= 1L;
	private Connection			conn;
	Statement					statement			= null;

	final static Logger logger = Logger.getLogger(MailActivationServlet.class);

	AuthUserService authUserService = null;
	
	public MailActivationServlet() {
		authUserService = new AuthUserServiceImpl();
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String key = request.getParameter("key");
		String url = "/userLogin.jsp";
		long userKey = Long.parseLong(key);
		boolean flag = activateUserByKey(userKey);

		if (flag == true) {
			RequestDispatcher rd = getServletContext().getRequestDispatcher(url);
			rd.forward(request, response);
		}
	}

	private boolean activateUserByKey(long userKey) {
		try {
			authUserService.activateUser(String.valueOf(userKey));
			return true;
		} catch (ServiceException e) {
			logger.error("Failed to activateUserByKey", e);
			return false;
		}
	}

}

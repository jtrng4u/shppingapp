package trng.imcs.store.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import trng.imcs.store.beans.User;
import trng.imcs.store.service.AuthUserService;
import trng.imcs.store.service.AuthUserServiceImpl;
import trng.imcs.store.service.ServiceException;

/**
 * Servlet implementation class LoginServlet
 */
@SuppressWarnings("serial")
public class LoginServlet extends HttpServlet {

	final static Logger logger = Logger.getLogger(LoginServlet.class);

	AuthUserService authUserService = null;
	private String authorizedForwardUrl = null;
	private String unauthorizedForwardUrl = null;
	private String inactiveUserForwardUrl = null;

	public LoginServlet() {
		authUserService = new AuthUserServiceImpl(); 
	}
	
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		authorizedForwardUrl = config.getInitParameter("authorizedForwardUrl");
		unauthorizedForwardUrl = config.getInitParameter("unauthorizedForwardUrl");
		inactiveUserForwardUrl = config.getInitParameter("inactiveUserForwardUrl");
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// helper function to check the credentials to the request.
		User user = checkCredentials(request);
		String forwardUrl = null;
		
		if (user == null) {
			forwardUrl = unauthorizedForwardUrl;
		} else {

			// checking whether the user is active i.e. Checking whether the
			// User Registration is confirmed or not.
			if (user.getActive() == 1) {
				startSession(request, response, user);
				forwardUrl = authorizedForwardUrl;
			} else {
				// the user registration is not yet confirmed.
				forwardUrl = inactiveUserForwardUrl;
			}
		}

		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(forwardUrl);
		dispatcher.forward(request, response);
	}

	// Function to start the Session and create Shopping Cart
	private void startSession(HttpServletRequest request, HttpServletResponse response, User user) {

		request.getSession(false).invalidate();
		HttpSession session = request.getSession(true);

		// Adding the username and user id to the session.
		session.setAttribute("username", user.getUserName());
		session.setAttribute("id", user.getUserId());
		session.setAttribute("user", user);
	}

	/* Function to check user credentials. */
	private User checkCredentials(HttpServletRequest request) {
		String userName = request.getParameter("username");
		String password = request.getParameter("password");

		try {
			return authUserService.getByUsername(userName, password);
		} catch (ServiceException e) {
			logger.error("Failed in checkCredentials", e);
			return null;
		}
	}

}

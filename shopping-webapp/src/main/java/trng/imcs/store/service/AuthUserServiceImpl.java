package trng.imcs.store.service;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

import trng.imcs.store.beans.User;
import trng.imcs.store.beans.UserActivation;
import trng.imcs.store.dao.DaoException;
import trng.imcs.store.dao.UserActivationDao;
import trng.imcs.store.dao.UserActivationDaoImpl;
import trng.imcs.store.dao.UserDao;
import trng.imcs.store.dao.UserDaoImpl;

public class AuthUserServiceImpl implements AuthUserService {

	private UserDao userDao;
	private UserActivationDao userActivationDao;

	public AuthUserServiceImpl() {
		userDao = new UserDaoImpl();
		userActivationDao = new UserActivationDaoImpl();
	}
	
	@Override
	public User saveUser(User user) throws ServiceException {
		user.setPassword(encode(user.getPassword()));
		try {
			return userDao.save(user);
		} catch (DaoException e) {
			throw new ServiceException("Failed to execute AuthUserServiceImpl::saveUser: ", e);
		}
	}

	@Override
	public  User getByUsername(String username, String password) throws ServiceException {
		String encodedPassword = encode(password); 
		User user;
		try {
			user = userDao.findUser(username, encodedPassword);
		} catch (DaoException e) {
			throw new ServiceException("Failed to execute AuthUserServiceImpl::getByUsername: ", e);
		}
		return user;
	}


	@Override
	public User getUserById(Integer userID) throws ServiceException {
		try {
			return userDao.find(userID);
		} catch (DaoException e) {
			throw new ServiceException("Failed to execute AuthUserServiceImpl::getUserById: ", e);
		}
	}
	
	private String encode(String normalString) {
		Base64.Encoder encoder = Base64.getEncoder();
		String encodedString = encoder.encodeToString(normalString.getBytes(StandardCharsets.UTF_8));
		return encodedString;

	}

	private String decode(String encodedString) {
		Base64.Decoder decoder = Base64.getDecoder();
		byte[] decodedByteArray = decoder.decode(encodedString);
		return new String(decodedByteArray);
	}

	@Override
	public boolean insertActivationDetails(Integer userID, String emailUID) throws ServiceException {
		User user;
		try {
			user = getUserById(userID);
			
			if (user == null) {
				return false;
			} else {
				return userDao.saveActivationDetails(userID, emailUID);
			}
		} catch (DaoException e) {
			throw new ServiceException("Failed to execute AuthUserServiceImpl::insertActivationDetails: ", e);
		}
		
	}

	@Override
	public boolean activateUser(String emailUID) throws ServiceException {
		UserActivation userActivation;
		try {
			userActivation = userActivationDao.getUserActivationDetails(emailUID);
			
			if (userActivation == null) {
				return false;
			} else {
				Integer userId = userActivation.getUserId();
				User user = getUserById(userId);
				user.setActive(1);
				saveUser(user);
				return true;
			}			
		} catch (DaoException e) {
			throw new ServiceException("Failed to execute AuthUserServiceImpl::activateUser: ", e);
		}
	}

}

package trng.imcs.store.service;

import trng.imcs.store.beans.User;

public interface AuthUserService {
	
	User saveUser(User user) throws ServiceException;

    User getByUsername(String userName, String password) throws ServiceException;
    
	User getUserById(Integer userID) throws ServiceException;
	
	boolean insertActivationDetails(Integer userID, String email) throws ServiceException;

	boolean activateUser(String emailUID) throws ServiceException;
}

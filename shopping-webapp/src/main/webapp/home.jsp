
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="css/shop.css">
	<title>Shopping Store</title>
</head>
<body>
	<div class="main">
		<div id="header">
			<div id="widgetBar">

				<div class="headerWidget">
					Welcome ${username}[ <a href="logout.jsp">Logout</a> ]
				</div>

				<div class="headerWidget">[ shopping cart widget ]</div>

			</div>

			<a href="#"> <img src="images/dota2 logo.jpg" id="logo"
				alt="Dota logo" align="left" />
			</a>
			<h5 id="logoText">Shopping Store</h5>
		</div>

		<div id="body">

			<h3>Welcome to Shopping Store</h3>
			<h6>
				This website is a simple shopping store which is built for learning
				purposes. <br />All the Item Names used in this website belongs to
				jtrng. As a huge jtrng Fan I create this application making
				that as the idea.
			</h6>

			<h5>
				Start Shopping by clicking <a href="displayitems">here</a>
			</h5>

		</div>
	</div>

	<%@ include file="footer.html" %>
</body>
</html>

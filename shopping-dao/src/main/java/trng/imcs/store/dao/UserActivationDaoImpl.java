package trng.imcs.store.dao;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import trng.imcs.store.beans.UserActivation;

public class UserActivationDaoImpl extends AbstractDao<UserActivation> implements UserActivationDao {

	public UserActivationDaoImpl() {
		super(UserActivation.class);
	}
	
	@Override
	public boolean saveActivationDetails(Integer userID, String emailUID) throws DaoException {
		UserActivation userActivation = new UserActivation();
		userActivation.setEmailKey(emailUID);
		userActivation.setUserId(userID);
		save(userActivation);
		return true;
	}

	@Override
	public UserActivation getUserActivationDetails(String emailUID) throws DaoException {
		EntityManager em = getEntityManager();
		try {
			Query query = em.createQuery("from UserActivation where emailKey= :emailKey");
			query.setParameter("emailKey", emailUID);
			return (UserActivation) query.getSingleResult();
		} catch (Exception e) {
			throw new DaoException("Exception on getUserActivationDetails", e);
		} finally {
			em.close();
		}
	}
}

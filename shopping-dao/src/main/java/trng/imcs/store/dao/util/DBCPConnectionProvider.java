package trng.imcs.store.dao.util;
import java.beans.PropertyVetoException;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;

public class DBCPConnectionProvider  {

    private static DataSource     datasource;
    private static BasicDataSource ds;

    static {
    	try {
			Properties connectionProps = getConnectionProperies();
			ds = new BasicDataSource();
			ds.setDriverClassName(connectionProps.getProperty("driver.name"));
			ds.setUrl(connectionProps.getProperty("connection.url"));
			ds.setUsername(connectionProps.getProperty("username"));
			ds.setPassword(connectionProps.getProperty("password"));
			
      
			// the settings below are optional -- dbcp can work with defaults
			ds.setMinIdle(5);
			ds.setMaxIdle(20);
			ds.setMaxOpenPreparedStatements(180);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    private DBCPConnectionProvider() throws IOException, SQLException, PropertyVetoException {
    }

    public static Connection getConnection() throws SQLException {
        return ds.getConnection();
    }
    
    private static Properties getConnectionProperies() throws IOException {
	    Properties connectionProps = new Properties();
	    FileInputStream in = new FileInputStream("C:\\Manohar\\workspace-training-advJava\\MyServletsShoppingApp\\shopping-dao\\src\\main\\resources\\defaultProperties.properties");
	    connectionProps.load(in);
	    in.close();
	    return connectionProps;
    }
}

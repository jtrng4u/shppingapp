package trng.imcs.store.dao;

public interface BaseDao<T> {
	
	T save(T entity) throws DaoException;
	
	T find(Object primaryKey) throws DaoException;
	
	boolean delete(Object primaryKey) throws DaoException;
}

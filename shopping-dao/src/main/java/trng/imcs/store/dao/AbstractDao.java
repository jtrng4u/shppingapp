package trng.imcs.store.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import org.apache.log4j.Logger;

import trng.imcs.store.dao.util.JPAUtil;

public class AbstractDao<T> implements BaseDao<T> {
	
	private Class<T> entityClass;
	final static Logger logger = Logger.getLogger(AbstractDao.class);

	public AbstractDao(Class<T> entityClass) {
		this.entityClass = entityClass;
	}
	
	@Override
	public T save(T entity) throws DaoException {
		EntityManager em = getEntityManager();
		EntityTransaction transaction = em.getTransaction();
		transaction.begin();
		try {
			em.persist(entity);
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
			throw new DaoException("Exception on save ", e);
		} finally {
			em.close();
		}
		
		return entity;
	}
	
	@Override
	public T find(Object primaryKey) throws DaoException {
		EntityManager em = getEntityManager();
		try {
			return em.find(entityClass, primaryKey);
		} catch (Exception e) {
			throw new DaoException("Exception on find:  ", e);
		} finally {
			em.close();
		}
	}

	@Override
	public boolean delete(Object primaryKey) throws DaoException {
		EntityManager em = getEntityManager();
		try {
			em.remove(find(primaryKey));
			return true;
		} catch (Exception e) {
			throw new DaoException("Exception on delete:  ", e);
		} finally {
			em.close();
		}
	}
	
	public EntityManager getEntityManager() {
		return JPAUtil.getEntityManagerFactory().createEntityManager();
	}

}

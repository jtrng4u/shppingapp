package trng.imcs.store.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import org.apache.log4j.Logger;

import trng.imcs.store.beans.User;

public class UserDaoImpl extends AbstractDao<User> implements UserDao {

	final static Logger logger = Logger.getLogger(UserDaoImpl.class);


	public UserDaoImpl() {
		super(User.class);
	}
	
	@Override
	public User findUser(String userName, String password) {
		EntityManager em = getEntityManager();
		User user = null;
		try {
			Query query = em.createQuery("from User where userName= :userName AND password= :password");
			query.setParameter("userName", userName);
			query.setParameter("password", password);
			user = (User) query.getSingleResult();
		} catch (Exception e) {
			logger.error("failed to findUser : ", e);
		} finally {
			em.close();
		}
		return user;
	}

	@Override
	public boolean saveActivationDetails(Integer userID, String emailUID) {
		EntityManager em = getEntityManager();
		EntityTransaction transaction = em.getTransaction();
		transaction.begin();
		try {
			Query query = em.createNativeQuery("insert into user_activation(userID, emailKey) values (:userId, :emailKey)");
			query.setParameter("userId", userID);
			query.setParameter("emailKey", emailUID);
			query.executeUpdate();
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
			logger.error("failed to saveActivationDetails : ", e);
		} finally {
			em.close();
		}
		
		return true;
	}
}

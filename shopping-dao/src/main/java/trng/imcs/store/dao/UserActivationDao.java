package trng.imcs.store.dao;

import trng.imcs.store.beans.UserActivation;

public interface UserActivationDao extends BaseDao<UserActivation> {
	
	boolean saveActivationDetails(Integer userID, String emailUID) throws DaoException;
	
	UserActivation getUserActivationDetails(String emailUID) throws DaoException;
}

package trng.imcs.store.dao;

import trng.imcs.store.beans.User;

public interface UserDao extends BaseDao<User> {
	
	User findUser(String username, String password) throws DaoException;
	
	boolean saveActivationDetails(Integer userID, String emailUID) throws DaoException;
}

package trng.imcs.store;

import trng.imcs.store.dao.util.HibernateUtils;
import trng.imcs.store.dao.util.JPAUtil;

public class App {

	public static void main(String[] args) {
		JPAUtil.getEntityManagerFactory().createEntityManager();
	}

}

package trng.imcs.store.beans;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table
@Data
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer		userId;
	private String	userName;
	private String	password;
	private String	email;
	private String	phoneNumber;
	private Integer		active;

	public User() {
		this.userId = 0;
	}

	public User(Integer userId, String userName, String password, String email, String phoneNumber, Integer active) {
		super();
		this.userId = userId;
		this.userName = userName;
		this.password = password;
		this.email = email;
		this.phoneNumber = phoneNumber;
		this.active = active;
	}


}

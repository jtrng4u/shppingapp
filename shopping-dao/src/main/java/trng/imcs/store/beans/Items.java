package trng.imcs.store.beans;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table
@Data
public class Items implements Serializable {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int					itemID;
	private String				itemName;
	private double				itemPrice;
	private String				itemQuality;
	private static int			referenceID;

	public Items() {

	}

	public Items(int int1, String string, double double1, String string2) {
		super();
		this.itemID = int1;
		this.itemName = string;
		this.itemPrice = double1;
		this.itemQuality = string2;
	}

	public Items(String string, double double1, String string2) {
		this.itemName = string;
		this.itemPrice = double1;
		this.itemQuality = string2;
	}


}

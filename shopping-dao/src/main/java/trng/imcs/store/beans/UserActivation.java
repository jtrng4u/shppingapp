package trng.imcs.store.beans;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name="user_activation")
@Data
public class UserActivation {

	@Id
	private Integer		userId;
	private String	emailKey;

	public UserActivation() {
		super();
	}
}
